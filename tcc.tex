\documentclass[12pt]{article}

\usepackage{graphicx}
\graphicspath{ {./figures/} }

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[margin=1in]{geometry}
\usepackage{textcomp}
\renewcommand{\arraystretch}{2}
\DeclareMathOperator{\diag}{diag}

\title{Partial Coherence and the Transmission Cross-Coefficient Matrix}
\author{Huy Le}
\date{January 2020}

\begin{document}

\maketitle

This follows \cite{hopkins1951,hopkins1952}.
\section{Derivation}
Suppose $U(\mathbf{x})$ is the complex amplitude of the field in front of the object resulted from a source element $d\sigma$, $E(\mathbf{x})$ is the object transmittance function, and $h(\mathbf{x})$ is the point spread function of the system. Bold variables represent \emph{two dimensional} position vectors.
\par
The image amplitude and intensity due to this source element are, respectively,
\begin{equation}
    \int U(\mathbf{x}_1)E(\mathbf{x}_1)h(\mathbf{x}-\mathbf{x}_1)d\mathbf{x}_1,
\end{equation}
\begin{equation}
    dI(\mathbf{x})=d\sigma\int\int U(\mathbf{x}_1)U^*(\mathbf{x}_2)E(\mathbf{x}_1)E^*(\mathbf{x}_2)h(\mathbf{x}-\mathbf{x}_1)h^*(\mathbf{x}-\mathbf{x}_2)d\mathbf{x}_1d\mathbf{x}_2.
\end{equation}
The total image intensity is
\begin{equation}
\begin{aligned}
    I(\mathbf{x})&=\int\int\left[\int U(\mathbf{x}_1)U^*(\mathbf{x}_2)d\sigma\right] E(\mathbf{x}_1)E^*(\mathbf{x}_2)h(\mathbf{x}-\mathbf{x}_1)h^*(\mathbf{x}-\mathbf{x}_2)d\mathbf{x}_1d\mathbf{x}_2,\\
    &=\int\int\gamma(\mathbf{x}_1-\mathbf{x}_2) E(\mathbf{x}_1)E^*(\mathbf{x}_2)h(\mathbf{x}-\mathbf{x}_1)h^*(\mathbf{x}-\mathbf{x}_2)d\mathbf{x}_1d\mathbf{x}_2,
\end{aligned}
\end{equation}
where \emph{the phase coherence factor} 
\begin{equation}
    \gamma(\mathbf{x}_1-\mathbf{x}_2)=\int U(\mathbf{x}_1)U^*(\mathbf{x}_2)d\sigma.
\end{equation}
Define the object spectrum $\tilde{E}(\boldsymbol{\xi})$ such that
\begin{equation}
    E(\mathbf{x})=\int\tilde{E}(\boldsymbol{\xi})e^{j2\pi\mathbf{x}\boldsymbol{\xi}}d\boldsymbol{\xi},
\end{equation}
and interchange the order of integration, the image intensity becomes
\begin{equation}
    I(\mathbf{x})=\int\int \tilde{E}(\boldsymbol{\xi}_1)\tilde{E}^*(\boldsymbol{\xi}_2) \left[\int\int \gamma(\mathbf{x}_1-\mathbf{x}_2) h(\mathbf{x}-\mathbf{x}_1)h^*(\mathbf{x}-\mathbf{x}_2) e^{j2\pi(\mathbf{x}_1\boldsymbol{\xi}_1-\mathbf{x}_2\boldsymbol{\xi}_2)} d\mathbf{x}_1d\mathbf{x}_2\right] d\boldsymbol{\xi}_1d\boldsymbol{\xi}_2.
\end{equation}

With a change of variable 
\begin{equation}
    \mathbf{X}_1=\mathbf{x}-\mathbf{x}_1,\;-\mathbf{X}_2=\mathbf{x}-\mathbf{x}_2.
\end{equation}
and, because $\mathbf{x}$ is a 2D variable, 
\begin{equation}
    d\mathbf{X_1}=d\mathbf{x}_1,\; d\mathbf{X}_2=d\mathbf{x}_2.
\end{equation}
the image intensity can be written as
\begin{equation}
\begin{aligned}
    I(\mathbf{x})=\int\int&\tilde{E}(\boldsymbol{\xi}_1)\tilde{E}^*(\boldsymbol{\xi}_2) \left[\int\int\gamma(-\mathbf{X}_1-\mathbf{X}_2) h(\mathbf{X}_1)h^*(-\mathbf{X}_2) e^{-j2\pi(\mathbf{X}_1\boldsymbol{\xi}_1+\mathbf{X}_2\boldsymbol{\xi}_2)}
     d\mathbf{X}_1d\mathbf{X}_2\right] \\
     &\times e^{j2\pi\mathbf{x}(\boldsymbol{\xi}_1-\boldsymbol{\xi}_2)} d\boldsymbol{\xi}_1d\boldsymbol{\xi}_2.
\end{aligned}
\end{equation}
The \emph{transmission cross-coefficient} (TCC) matrix is defined by the term in the above bracket
\begin{equation}
    C(\boldsymbol{\xi}_1,\boldsymbol{\xi}_2)=\int\int\gamma(-\mathbf{X}_1-\mathbf{X}_2) h(\mathbf{X}_1)h^*(-\mathbf{X}_2) e^{-j2\pi(\mathbf{X}_1\boldsymbol{\xi}_1+\mathbf{X}_2\boldsymbol{\xi}_2)}
     d\mathbf{X}_1d\mathbf{X}_2.
\end{equation}
To simplify this matrix, use the following theorem of Fourier transform
\begin{equation}
    \int\int f(-x_1-x_2)g(x_1)h(x_2)dx_1dx_2=\int F(\xi)G(\xi)H(\xi)d\xi,
\end{equation}
for any three functions $f,g,h$ and their transforms $F,G,H$. This theorem is easily seen as
\begin{equation}
    \int\int f(-x_1-x_2)g(x_1)h(x_2)dx_1dx_2=(f\otimes g\otimes h)(0)=\int \mathcal{F}\{f\otimes g\otimes h\}=\int F(\xi)G(\xi)H(\xi)d\xi.
\end{equation}
To apply the theorem, write 
\begin{equation}
\begin{aligned}
    C(\boldsymbol{\xi}_1,\boldsymbol{\xi}_2)&=\int\int\gamma(-\mathbf{X}_1-\mathbf{X}_2) \left[h(\mathbf{X}_1)e^{-j2\pi\mathbf{X}_1\boldsymbol{\xi}_1}\right]
    \left[h^*(-\mathbf{X}_2) e^{-j2\pi\mathbf{X}_2\boldsymbol{\xi}_2}\right]
     d\mathbf{X}_1d\mathbf{X}_2,\\
     &=\int \Gamma(\mathbf{Z})H(\mathbf{Z}+\boldsymbol{\xi}_1)H^*(\mathbf{Z}+\boldsymbol{\xi}_2)d\mathbf{Z},
\end{aligned}
\end{equation}
where $\Gamma$ and $H$ are the Fourier transforms of $\gamma$ and $h$ respectively.
\par
Written in term of the TCC, the image intensity is
\begin{equation}
    I(\mathbf{x})=\int\int\tilde{E}(\boldsymbol{\xi}_1)\tilde{E}^*(\boldsymbol{\xi}_2) 
     C(\boldsymbol{\xi}_1,\boldsymbol{\xi}_2)
     e^{j2\pi\mathbf{x}(\boldsymbol{\xi}_1-\boldsymbol{\xi}_2)} d\boldsymbol{\xi}_1d\boldsymbol{\xi}_2.
\end{equation}

%\section{Discretization}
%Let's try to discretize an integral of the form similar to the image intensity 
%\begin{equation}
%\begin{aligned}
%    G(s_1,s_2)&=\int\int a(x_1)g(x_1,x_2)b(x_2)e^{j2\pi(s_1x_1-s_2x_2)}dx_1dx_2,\\
%    &=\int a(x_1)\left[\int g(x_1,x_2)b(x_2)e^{-j2\pi s_2x_2}dx_2\right]e^{j2\pi s_1x_1}dx_1,\\
%    &=\int a(x_1)K(x_1,s_2)e^{j2\pi s_1x_1}dx_1
%\end{aligned}
%\end{equation}
%Using the notation $v_n$ with the subscript representing the dimension of the vector $v\in \mathbb{C}^n$, $\mathcal{F}$ for the Fourier transform matrix, and $\diag v$ for the diagonal matrix whose entries are elements of $v$, the discretized form of the integral is
%\begin{equation}
%    G_{ns_1\times ns_2}=\mathcal{F}^{-1}_{ns_1\times nx_1} \left(\diag a\right)_{nx_1 \times nx_1}K_{nx_1\times ns_2},
%\end{equation}
%where
%\begin{equation}
%    \left(K_{nx_1\times ns_2}\right)^T=\mathcal{F}_{ns_2\times nx_2}\left[g_{nx_1\times nx_2}(\diag b)_{nx_2\times nx_2}\right]^T,
%\end{equation}
%The procedure is 
%\begin{itemize}
%    \item Element-wise multiply vector $b_{nx_2}$ with $nx_1$ rows of the matrix $g_{nx_1\times nx_2}$.
%    \item Perform $nx_1$ 1D Fourier transforms on the resulting rows to get the rows of $K_{nx_1\times ns_2}$.
%    \item Element-wise multiply vector $a_{nx_1}$ with $ns_2$ columns of $K_{nx_1\times ns_2}$.
%    \item Perform $ns_2$ 1D Fourier transforms on the resulting columns to get the columns of $G_{ns_1\times ns_2}$. 
%\end{itemize}

\bibliographystyle{unsrt}
\bibliography{ref}

\end{document}
